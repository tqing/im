import { basicConfigService } from '@/api/basicConfig'
import { onMounted } from 'vue'

export default function options() {
  const sceneOptions = [
    {
      label: '计划停电前',
      value: 1,
    },
    {
      label: '停电中',
      value: 2,
    },
    {
      label: '复电后',
      value: 3,
    },
  ]
  const residenceOptions = ref([])
  const getAllCommunity = () => {
    basicConfigService.getAllCommunity({ data: {} }).then((res) => {
      let data = res.data.map((e) => {
        return {
          label: e.communityName,
          value: e.communityId,
        }
      })
      residenceOptions.value.push(...data)
    })
  }
  onMounted(() => {
    getAllCommunity()
  })
  return { sceneOptions, residenceOptions }
}
