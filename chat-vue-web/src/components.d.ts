/* eslint-disable */
/* prettier-ignore */
// @ts-nocheck
// Generated by unplugin-vue-components
// Read more: https://github.com/vuejs/core/pull/3399
export {}

declare module 'vue' {
  export interface GlobalComponents {
    AreaTag: typeof import('./components/AssociatedArea/areaTag.vue')['default']
    AssociatedArea: typeof import('./components/AssociatedArea/index.vue')['default']
    AssociatedCommunity: typeof import('./components/AssociatedCommunity/index.vue')['default']
    AssociatedLine: typeof import('./components/AssociatedLine/index.vue')['default']
    AuditStatusName: typeof import('./components/AuditStatusName.vue')['default']
    AutocompletePage: typeof import('./components/autocompletePage.vue')['default']
    BaseDialog: typeof import('./components/base-dialog.vue')['default']
    BaseForm: typeof import('./components/base-form.vue')['default']
    BaseTable: typeof import('./components/base-table.vue')['default']
    CateEditForm: typeof import('./components/LeftTree/CateList/CateEditForm.vue')['default']
    CateList: typeof import('./components/LeftTree/CateList/index.vue')['default']
    CommunityTags: typeof import('./components/AssociatedCommunity/communityTags.vue')['default']
    ContentHeader: typeof import('./components/content-header.vue')['default']
    CorrelationCommunity: typeof import('./components/AssociatedCommunity/correlationCommunity.vue')['default']
    EditUpload: typeof import('./components/EditUpload.vue')['default']
    ElAside: typeof import('element-plus/es')['ElAside']
    ElButton: typeof import('element-plus/es')['ElButton']
    ElCheckbox: typeof import('element-plus/es')['ElCheckbox']
    ElCheckboxGroup: typeof import('element-plus/es')['ElCheckboxGroup']
    ElCol: typeof import('element-plus/es')['ElCol']
    ElCollapse: typeof import('element-plus/es')['ElCollapse']
    ElCollapseItem: typeof import('element-plus/es')['ElCollapseItem']
    ElConfigProvider: typeof import('element-plus/es')['ElConfigProvider']
    ElContainer: typeof import('element-plus/es')['ElContainer']
    ElDatePicker: typeof import('element-plus/es')['ElDatePicker']
    ElDescriptions: typeof import('element-plus/es')['ElDescriptions']
    ElDescriptionsItem: typeof import('element-plus/es')['ElDescriptionsItem']
    ElDialog: typeof import('element-plus/es')['ElDialog']
    ElDropdown: typeof import('element-plus/es')['ElDropdown']
    ElDropdownItem: typeof import('element-plus/es')['ElDropdownItem']
    ElDropdownMenu: typeof import('element-plus/es')['ElDropdownMenu']
    ElForm: typeof import('element-plus/es')['ElForm']
    ElFormItem: typeof import('element-plus/es')['ElFormItem']
    ElHeader: typeof import('element-plus/es')['ElHeader']
    ElIcon: typeof import('element-plus/es')['ElIcon']
    ElImage: typeof import('element-plus/es')['ElImage']
    ElInput: typeof import('element-plus/es')['ElInput']
    ElInputNumber: typeof import('element-plus/es')['ElInputNumber']
    ElLink: typeof import('element-plus/es')['ElLink']
    ElMain: typeof import('element-plus/es')['ElMain']
    ElMenu: typeof import('element-plus/es')['ElMenu']
    ElMenuItem: typeof import('element-plus/es')['ElMenuItem']
    ElMenuItemGroup: typeof import('element-plus/es')['ElMenuItemGroup']
    ElOption: typeof import('element-plus/es')['ElOption']
    ElPagination: typeof import('element-plus/es')['ElPagination']
    ElRadio: typeof import('element-plus/es')['ElRadio']
    ElRadioButton: typeof import('element-plus/es')['ElRadioButton']
    ElRadioGroup: typeof import('element-plus/es')['ElRadioGroup']
    ElRow: typeof import('element-plus/es')['ElRow']
    ElScrollbar: typeof import('element-plus/es')['ElScrollbar']
    ElSelect: typeof import('element-plus/es')['ElSelect']
    ElSwitch: typeof import('element-plus/es')['ElSwitch']
    ElTable: typeof import('element-plus/es')['ElTable']
    ElTableColumn: typeof import('element-plus/es')['ElTableColumn']
    ElTabPane: typeof import('element-plus/es')['ElTabPane']
    ElTabs: typeof import('element-plus/es')['ElTabs']
    ElTag: typeof import('element-plus/es')['ElTag']
    ElText: typeof import('element-plus/es')['ElText']
    ElTimePicker: typeof import('element-plus/es')['ElTimePicker']
    ElTooltip: typeof import('element-plus/es')['ElTooltip']
    ElUpload: typeof import('element-plus/es')['ElUpload']
    ExecuteStatus: typeof import('./components/ExecuteStatus.vue')['default']
    FielsList: typeof import('./components/FielsList.vue')['default']
    FilesDetals: typeof import('./components/filesDetals.vue')['default']
    ImportFile: typeof import('./components/ImportFile/index.vue')['default']
    IsDisableStatus: typeof import('./components/isDisableStatus.vue')['default']
    JqrUpload: typeof import('./components/JqrUpload.vue')['default']
    LabelDialog: typeof import('./components/labelDialog.vue')['default']
    LeftTreeSlot: typeof import('./components/LeftTree/leftTreeSlot.vue')['default']
    LineTags: typeof import('./components/AssociatedLine/lineTags.vue')['default']
    NewBaseSelect: typeof import('./components/NewSelectArea/NewBaseSelect.vue')['default']
    NewSelectArea: typeof import('./components/NewSelectArea/NewSelectArea.vue')['default']
    NewUpload: typeof import('./components/NewUpload.vue')['default']
    Pages: typeof import('./components/Pages.vue')['default']
    RouterLink: typeof import('vue-router')['RouterLink']
    RouterView: typeof import('vue-router')['RouterView']
    SelectArea: typeof import('./components/selectArea.vue')['default']
    'SelectArea copy': typeof import('./components/selectArea copy.vue')['default']
    SelectedTags: typeof import('./components/select-pop/selectedTags.vue')['default']
    SelectFriends: typeof import('./components/selectFriends/index.vue')['default']
    SelectGroup: typeof import('./components/selectGroup/index.vue')['default']
    SelectLabel: typeof import('./components/selectLabel.vue')['default']
    SelectLine: typeof import('./components/selectLine.vue')['default']
    SelectList: typeof import('./components/AssociatedCommunity/selectList.vue')['default']
    SelectMaterial: typeof import('./components/selectMaterial/index.vue')['default']
    SelectPage: typeof import('./components/selectPage.vue')['default']
    SelectPageMultiple: typeof import('./components/selectPageMultiple.vue')['default']
    SelectPop: typeof import('./components/select-pop/index.vue')['default']
    SelectSite: typeof import('./components/selectSite.vue')['default']
    SendStatusName: typeof import('./components/SendStatusName.vue')['default']
    SortTable: typeof import('./components/sortTable.vue')['default']
    Tooltip: typeof import('./components/tooltip.vue')['default']
    TypesList: typeof import('./components/LeftTree/TypesList/index.vue')['default']
    TypesListForm: typeof import('./components/LeftTree/TypesList/TypesListForm.vue')['default']
    WxRobotCode: typeof import('./components/wxRobotCode.vue')['default']
  }
  export interface ComponentCustomProperties {
    vLoading: typeof import('element-plus/es')['ElLoadingDirective']
  }
}
