export default {
  userInfo: (state: any) => state.user.userInfo,
  menuList: (state: any) => state.menus.menuList,
  activeMenu: (state: any) => state.menus.activeMenu,
  tableHeight: (state: any) => state.table.tableHeight,
  cateList: (state: any) => state.cates.cateList,
}
