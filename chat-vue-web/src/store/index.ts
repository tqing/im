import getters from './getters'
import user from './modules/user'
import menus from './modules/menus'
import table from './modules/table'
import cates from './modules/cates'
// export default store;
import { createStore } from 'vuex'

export default createStore({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    user,
    menus,
    table,
    cates,
  },
  getters,
})
