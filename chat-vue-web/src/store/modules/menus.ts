const state = {
  menuList: [],
  activeMenu: {},
}
const mutations = {
  SET_MENULIST(state: { menuList: any }, payload: any) {
    state.menuList = payload
  },
  SET_ACTIVEMENU(state: { activeMenu: any }, payload: any) {
    state.activeMenu = payload
  },
}
const actions = {
  setMenuList(context, data) {
    context.commit('SET_MENULIST', data)
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
}
