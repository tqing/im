const state = {
  cateList: [],
}
const mutations = {
  SET_CATELIST(state: { cateList: any }, payload: any) {
    state.cateList = payload
  },
}
export default {
  namespaced: true,
  state,
  mutations,
}
