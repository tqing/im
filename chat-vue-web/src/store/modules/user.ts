const state = {
    userInfo: 100,
}

const mutations = {
    SET_USERINFO(state: { userInfo: any; }, payload: any) {
        state.userInfo = payload
    }
};

const actions = {};

export default {
    namespaced: true,
    state,
    mutations,
    actions
};