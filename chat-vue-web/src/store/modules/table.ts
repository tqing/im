const state = {
  tableHeight: 0,
}
const mutations = {
  SET_TABLEHEIGHT(state: { tableHeight: any }, payload: any) {
    state.tableHeight = payload
  },
}

export default {
  namespaced: true,
  state,
  mutations,
}
