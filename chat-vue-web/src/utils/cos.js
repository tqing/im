// import "../../../public/aws-sdk.min"

export default {
  methods: {
    fileCOSChange(file) {
      const files = file.target.files[0]
      let ContentType = 'text/plain'
      const fileName = new Date().getTime() + '' + files.name
      var params = {
        Bucket: this.bucket,
        Key: this.fileName + '/' + fileName,
        Body: files,
        Region: 'ap-chongqing',
        ACL: 'public-read', // 初始化acl权限，默认为private，"private"|"public-read"|"public-read-write"
        ContentType: ContentType, // 设置contentType, 默认是application/octet-stream
      }

      this.cos.uploadFile(params, (err, data) => {
        console.log('erro====', err)
        if (err) {
          console.log(err)
          this.$emit('err', err)
        } else {
          let file = {
            name: files.name,
            url: `https://${this.endpoint}/${this.bucket}/${fileName}`,
          }
          if (this.listType === 'picture' || this.listType === 'avatar') {
            this.filePictureChange(file)
          } else if (this.listType === 'text') {
            this.fileTextChange(file)
          } else if (this.listType === 'picture-list') {
            this.fileTextChange(file)
          }
        }
      })
    },
  },
}
