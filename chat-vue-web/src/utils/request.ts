import axios from 'axios'
import { getToken } from './auth'
const service = axios.create({
  baseURL: import.meta.env.VITE_BASE_URL, // url = base url + request url
  //baseURL: 'http://test-gateway.lamsi.cn',
  // baseURL: 'http://129.28.151.33:8091',
  timeout: 200000, // request timeout
})

service.interceptors.request.use(
  (config: any) => {
    // console.log(process.env.VITE_BASE_URL);
    let token = getToken()
    if (import.meta.env.VITE_USER_NODE_ENV === 'development') {
      token ='eyJ0eXAiOiJKV1QiLCJhbGciOiJSU0EifQ==./3ul3nR+7BjYm9atnfxUmlA13Wu7a+3wos3ZUJQVfb+OY7TFvo3ioM8O78x463mosTF4kpQocT9O+IQSK7EkSAeuc+N30JxEKy4RPYQL2iP2tCht+aJz2IM7z5Z6Z/N5D3a4bU9P/mJjHT/djf/X+dwXEyUGGpui0kGyrK8MAa3o87ieI0769X9rfcvFrt0Aobkf51qjpRNYZMMEYEYxRSUgZlRsEGaKwjh9QEBfSNU=.79da202f839225b5bb744cb5e0c0b35ff2313466'
      //'eyJ0eXAiOiJKV1QiLCJhbGciOiJSU0EifQ==.dmbQVnxRGVO/B8WrQeWBmoxl/4MUEq3BDiMA8MV87W6OY7TFvo3ioM8O78x463moN356XPRh/0FkcoDKsWF6yAeuc+N30JxEKy4RPYQL2iPZFSM8PtVcegkMW9Z50p4AD3a4bU9P/mJjHT/djf/X+dwXEyUGGpui0kGyrK8MAa373/q/oAPE8NO+REVVBWfSobkf51qjpRNYZMMEYEYxRS8oNFpzUlFMEfAnMMFlCu8=.fe7ce57c76dc0de207c2cb945c0a0dc7dc00472f'
        'eyJ0eXAiOiJKV1QiLCJhbGciOiJSU0EifQ==.JVQeuuc0+7w5kO0rN+mCv9YTtNxG17mjZJmWopaUKKqOY7TFvo3ioM8O78x463mo63hGyCPfrhiDCIV6X0UE1weuc+N30JxEKy4RPYQL2iNUfio7zqhozThhoaAYtWmn5wsFhBUOUblBV/o+UQrSJNwXEyUGGpui0kGyrK8MAa1rdp6d0VC99q1SY0PGg5nCsQVETsOxqRuXDPFSL7Ailc9pxHfAzF7bM1HT6R149dU=.f96d07b3618b626c4e4332ec8d5db763a8ff3aab'
    }
    if (token) {
      config.headers['accessToken'] = token
    }
    // config.headers['appId'] = 'tongyuan-vue-supply-order-web';
    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

service.interceptors.response.use(
  (response) => {
    const data = response.data
   if(response.headers['content-type']?.indexOf('application/vnd.ms-excel')>-1){
    return data
   }
    if(data.type=='application/vnd.ms-excel'){
      return data
    }
    if (data.success) {
      return data
    } else {
      ElMessage({
        showClose: true,
        message: data.description,
        type: 'error',
      })
      return Promise.reject(data.message)
    }
  },
  (error) => {
    ElMessage({
      showClose: true,
      message: error.description,
      type: 'error',
    })
    return Promise.reject(error)
  }
)

export default service
