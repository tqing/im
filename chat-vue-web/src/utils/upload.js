let url = BASE_URL + '/common-api/common/file/sts'
console.log(url)
const res = await fetch(url, {
  method: 'post',
  headers: {
    accessToken: token,
    // accessToken: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSU0EifQ==.Y9n6d5nXJAqBgsAnf/QtlAD7ooLLQc86ZU2gQcQ8j6qC9FSPth5eplJYGA0qLX6Gf/CDcuNiByPIOHQtmeFveJ5JUtw1/vUU++WY0vYBeooYu6d6uM1Az3YtyMzX4ZyosZWkQyfw9awiDbJV6KtGmB8nYPmot+RMQoUrof8PAIPFaZ7Lw+ay6LguVkL4ynkPuEI2X4xJpZyIeiq1c3qUdQ==.282f412eee700167d8a215ef4d9c80cdea944ecd'
  },
}).then((res) => res.json())
setCookie('bucket', res.data.bucket)
setCookie('endpoint', res.data.endpoint)
setCookie('accessKeyId', res.data.accessKeyId)
setCookie('secretAccessKey', res.data.secretAccessKey)
setCookie('sessionToken', res.data.sessionToken)
setCookie('expireEndTime', res.data.expireEndTime)
setCookie('StartTime', res.data.StartTime)
setCookie('fileSystemCloudProvider', res.data.fileSystemCloudProvider)
resolve(res.data)
