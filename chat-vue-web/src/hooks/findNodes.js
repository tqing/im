// 找到tree下面符合条件的node
export const findNode = (data, key, id, childrenKey = 'children') => {
    const find = data.find(({ [key]: k }) => k == id)
    if (find) return find
    for (const item of data) {
      if (item[childrenKey] && item[childrenKey].length > 0) {
        const find = findNode(item[childrenKey], key, id, childrenKey)
        if (find) {
          return find
        }
      }
    }
    return {}
  }
  // 找到tree的所有子集id
  export const findChildrenIds = (data, key, childrenKey = 'children') => {
    let ids = []
    const traverse = (node) => {
      ids.push(node[key])
      if (node[childrenKey] && node[childrenKey].length > 0) {
        node[childrenKey].forEach((child) => {
          traverse(child)
        })
      }
    }
    traverse(data)
    return ids
  }
  