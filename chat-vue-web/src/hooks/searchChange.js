import { basicConfigService } from '@/api/basicConfig'
import { lineService } from '@/api/line'
import { powerService } from '@/api/power'

export default function () {
    const lineIdList = ref([])
    const areaList = ref([])
    const powerSupplyIdList = ref([])
    const filterChildren = (data) => {
        const traverse = (nodes) => {
            nodes.forEach((item) => {
                item.label = item.powerName
                item.value = item.powerId
                if (item.children && item.children.length > 0) {
                    traverse(item.children)
                }
            })
        }
        traverse(data)
        return data
    }
    const getPowerSupplyIdList = () => {
        powerService
            .getPowerPageList({ data: {}, pageNumber: 1, pageSize: 99999 })
            .then((res) => {
                powerSupplyIdList.value = filterChildren(res.data.content)
            })
    }
    const getLineList = (id) => {
        lineService.getLineList({ data: { powerId: id } }).then((res) => {
            lineIdList.value = res.data.map((n) => {
                return {
                    label: n.lineName,
                    value: n.lineId,
                }
            })
        })
    }
    const getAreaList = (powerId, lineId) => {
        basicConfigService
            .queryGridAreaDtoList({
                data: {
                    powerId,
                    lineId,
                },
            })
            .then((res) => {
                areaList.value = res.data.map((e) => {
                    return {
                        label: e.areaName,
                        value: e.areaId,
                    }
                })
            })
    }
    const changePower = (id) => {
        getLineList(id)
        getAreaList(id)
    }
    const changeLine = (powerId, lineId) => {
        getAreaList(powerId, lineId)
    }

    return {
        lineIdList,
        areaList,
        getLineList,
        getAreaList,
        changePower,
        changeLine,
        powerSupplyIdList,
        getPowerSupplyIdList,
    }
}
