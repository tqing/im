export const getRobotState = computed(() => {
  return (key) => {
    switch (key) {
      case 1:
        return '正常'
      case 2:
        return '异常：机器人未登录'
      case 3:
        return '异常：微信未登录，与网格经理/其它机器人微信账号冲突'
      case 4:
        return '异常：与网格经理/其它机器人微信账号冲突'
      default:
        break
    }
  }
})
export const getCodeState = computed(() => {
  return (key) => {
    switch (key) {
      case 0:
        return '未知'
      case 1:
        return '已取消登录，请使用机器人微信重新扫码登录'
      case 2:
        return '请使用机器人微信扫码登录'
      case 3:
        return '请在手机上确认登录'
      case 4:
        return '请在手机上确认登录'
      case 5:
        return '二维码已刷新，请使用机器人微信扫码登录'
      default:
        break
    }
  }
})
export const getAuditStatus = computed(() => {
  return (key) => {
    switch (key) {
      case 0:
        return {
          color: '#02a7f0fe',
          text: '待审核',
        }
      case 1:
        return {
          color: '#70b603fe',
          text: '审核通过',
        }
      case 2:
        return {
          color: '#d9001bfe',
          text: '审核不通过',
        }
      default:
        return {
          color: '',
          text: '/',
        }
    }
  }
})

export const getSendStatus = computed(() => {
  return (key) => {
    switch (key) {
      case 1:
        return {
          color: '#70b603fe',
          text: '已发送',
        }
      case 0:
        return {
          color: '#7f7f7ffe',
          text: '未发送',
        }
      default:
        return {}
    }
  }
})
