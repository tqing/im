export default function openMsgBox(obj = {}, callBack1, callBack2) {
  let tips = '确定要删除吗?',
    title = '确认提示'
  ElMessageBox.confirm(obj.tips || tips, obj.title || title, {
    confirmButtonText: '确定',
    cancelButtonText: '取消',
    type: 'warning',
  })
    .then(() => {
      callBack1().then((res) => {
        ElMessage({
          message: '操作成功',
          type: 'success',
        })
        callBack2()
      })
    })
    .catch((err) => {
      console.log(err)
    })
}
