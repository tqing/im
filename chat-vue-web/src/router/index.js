import { createRouter, createWebHistory } from 'vue-router'
import { qiankunWindow } from 'vite-plugin-qiankun/dist/helper'
import routes from './routeConfig'
console.log(process.env)
const router = createRouter({
  // 微服务启动时：路由前缀 + 基座父应用的规则
  // 独自启动时： 路由为： /
  history: createWebHistory(
    qiankunWindow.__POWERED_BY_QIANKUN__
      ? (process.env.VITE_ROUTER_BASE_URL || '') + process.env.VITE_QIANKUN_PATH
      : '/'
  ),
  // base: qiankunWindow.__POWERED_BY_QIANKUN__
  //   ? '/index/robot'
  //   : process.env.VITE_BASE_URL,
  routes: [
    {
      path: '/',
      redirect: '/login',
    },
    
    ...routes,
  ],
})

// 动态修改路由过度动画
// router.afterEach((to, from) => {
//   // const toDepth = to.path.split('/').length
//   // const fromDepth = from.path.split('/').length

//   if (to.path == '/login') {
//     to.meta.transitionName = 'down'
//     from.meta.transitionName = 'opt'
//   } else if (from.path == '/login') {
//     from.meta.transitionName = 'down'
//     to.meta.transitionName = 'up'
//   } else {
//     // to.meta.transitionName = toDepth <= fromDepth ? 'slide-left' : 'slide-right'
//     // to.meta.transitionName = 'slide-right'
//   }
// })
export default router
