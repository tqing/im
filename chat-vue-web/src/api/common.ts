import request from '@/utils/request'
/**
 *
 * @class CommonService
 */

class CommonService {
  // 省市区
  getDistrictList(data) {
    return request({
      url: '/common-api/common/sysarea/getByPid',
      method: 'POST',
      data,
    })
  }
  // 根据小区ids, 查询微信群-小区关联关系
  getGroupCommunityInfoDtoList(data) {
    return request({
      url: '/robot-api/admin/groupCommunity/getGroupCommunityInfoDtoList',
      method: 'POST',
      data,
    })
  }

  // 字典
  getListByParentKey(data) {
    return request({
      url: '/common-api/common/dictionary/selectDictionarysByKey',
      method: 'POST',
      data: { data: { dictionaryKey: data.data } },
    })
  }
  getListByParentKeyList(data) {
    return request({
      url: '/common-api/common/dictionary/selectDictionarysByKey',
      method: 'POST',
      data,
    })
  }
  // 所有字典分类
  getAllDictionaryList(data) {
    return request({
      url: '/robot-api/common/dictionary/getAllDictionaryList',
      method: 'POST',
      data,
    })
  }
  // 新增字典分类
  addDictionary(data) {
    return request({
      url: '/robot-api/common/dictionary/addDictionary',
      method: 'POST',
      data,
    })
  }
  // 编辑字典分类
  editDictionary(data) {
    return request({
      url: '/robot-api/common/dictionary/editDictionary',
      method: 'POST',
      data,
    })
  }
  // 删除字典分类
  delDictionary(data) {
    return request({
      url: '/robot-api/common/dictionary/del',
      method: 'POST',
      data,
    })
  }

  // 上传
  uploadFiles(url) {
    return request({
      url,
      method: 'POST',
      // data,
      // headers: {
      //   'Content-type': 'multipart/form-data',
      // },
    })
  }
  // 字典
  getRobotListByParentKey(data) {
    return request({
      url: '/robot-api/common/dictionary/selectDictionarysByKey',
      method: 'POST',
      data: { data: { dictionaryKey: data.data } },
    })
  }
  /* 日志 */
  getLogList(data) {
    return request({
      url: '/robot-api/admin/sys/log/pageList',
      method: 'POST',
      data,
    })
  }
}

export const commonService = new CommonService()
