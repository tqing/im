import request from '@/utils/request';
class H5Service {
  createWorkOrder(data: any) {
    return request({
      url: '/robot-api/h5/workorder/create',
      method: 'POST',
      data,
    });
  }
}
export const h5Service = new H5Service();
