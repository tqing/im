import request from '@/utils/request'
class FileService {

    upLoad(data: any,url) {
        return request({
            url,
            method: 'POST',
            data,
            headers:{
                'Content-Type': 'multipart/form-data',
            }
            
        })
    }
 
   
}
export const fileService = new FileService()