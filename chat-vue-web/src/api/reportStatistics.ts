import request from '@/utils/request';
/**
 *
 * @class ReportStatisticsService
 */

class ReportStatisticsService {
  // 舆情报表统计
  opinionKeyRecordPageList(data) {
    return request({
      url: '/robot-api/admin/opinionKeyRecord/pageList',
      method: 'POST',
      data,
    });
  }
  opinionKeyRecordPageListDownload(data) {
    return request({
      url: '/robot-api/admin/opinionKeyRecord/pageList/download',
      method: 'POST',
      data,
      responseType: 'blob',
    });
  }
  // 查询触发群聊
  opinionKeyRecordGetByIdStr(data) {
    return request({
      url: '/robot-api/admin/opinionKeyRecord/getByIdStr',
      method: 'POST',
      data,
    });
  }
  //下载
  downLoad(data) {
    return request({
      url: '/robot-api/admin/opinionKeyRecord/pageList/download',
      method: 'POST',
      data,
      responseType: 'blob',
    });
  }
  warnLedgerPageList(data) {
    return request({
      url: '/robot-api/admin/warnLedger/pageList',
      method: 'POST',
      data,
    });
  }

  warnLedgerPageListDownload(data) {
    return request({
      url: '/robot-api/admin/warnLedger/pageList/download',
      method: 'POST',
      data,
      responseType: 'blob',
    });
  }
  // 私聊机器人台账列表
  robotLedgerPageList(data) {
    return request({
      url: '/robot-api/admin/manager/managerAndRobotMessageStatistics',
      method: 'POST',
      data,
    });
  }

  // 私聊机器人台账列表导出
  robotLedgerPageListDownload(data) {
    return request({
      url: '/robot-api/admin/manager/managerAndRobotMessageStatisticsDownload',
      method: 'POST',
      data,
      responseType: 'blob',
    });
  }
  // 私聊机器人详情导出
  robotLedgerPageListDetailDownload(data) {
    return request({
      url: '/robot-api/admin/wx/group/record/pageManagerAndRobotMessageDownload',
      method: 'POST',
      data,
      responseType: 'blob',
    });
  }
}

export const reportStatisticsService = new ReportStatisticsService();
