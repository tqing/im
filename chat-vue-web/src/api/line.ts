import request from '@/utils/request'
class LineService {

    getLinePageList(data: any) {
        return request({
            url: '/robot-api/admin/line/pageList',
            method: 'POST',
            data,
        })
    }
    getLineList(data: any) {
        return request({
            url: '/robot-api/admin/line/queryLineDtoList',
            method: 'POST',
            data,
        })
    }
    addLine(data: any) {
        return request({
            url: '/robot-api/admin/line/save',
            method: 'POST',
            data,
        })
    }
    updateLine(data: any) {
        return request({
            url: '/robot-api/admin/line/update',
            method: 'POST',
            data,
        })
    }
    delLine(data: any) {
        return request({
            url: '/robot-api/admin/line/batchDel',
            method: 'POST',
            data,
        })
    }
    /* 台区分页查询new */
    getGridPageList(data: any) {
        return request({
            url: '/robot-api/admin/area/newPageList',
            method: 'POST',
            data,
        })
    }
    addGrid(data: any) {
        return request({
            url: '/robot-api/admin/area/newBatchSave',
            method: 'POST',
            data,
        })
    }
    updateGrid(data: any) {
        return request({
            url: '/robot-api/admin/area/update',
            method: 'POST',
            data,
        })
    }
    delGrid(data: any) {
        return request({
            url: '/robot-api/admin/area/delete',
            method: 'POST',
            data,
        })
    }
    exportGrid(data: any) {
        return request({
            url: '/robot-api/admin/area/areaImport',
            method: 'POST',
            data,
        })
    }
    failAreaDownload(data: any) {
        return request({
            url: '/robot-api/admin/area/failAreaDownload',
            method: 'POST',
            data,
            responseType: 'blob'
        })
    }
    /* 台区列表查询 */
    queryByQueryDto(data: any) {
        return request({
            url: '/robot-api/admin/area/queryByQueryDto',
            method: 'POST',
            data,
           
        })
    }
    queryLineDto(data: any) {
        return request({
            url: '/robot-api/admin/line/queryByQueryDto',
            method: 'POST',
            data,
           
        })
    }
    getByAreaQueryByArea(data: any) {
        return request({
            url: '/robot-api/admin/area/getByAreaQueryByArea',
            method: 'POST',
            data,
           
        })
    }
    getByAreaQueryByLine(data: any) {
        return request({
            url: '/robot-api/admin/area/getByAreaQueryByLine',
            method: 'POST',
            data,
           
        })
    }
    getByAreaQueryByPower(data: any) {
        return request({
            url: '/robot-api/admin/area/getByAreaQueryByPower',
            method: 'POST',
            data,
           
        })
    }
      // 小区分页
  getAllCommunity(data) {
    return request({
      url: '/robot-api/admin/community/getAllCommunity',
      method: 'POST',
      data,
    })
  }
}
export const lineService = new LineService()