import request from '@/utils/request'

export function getServiceList(data: any) {
  return request({
    url: 'order-web-api/supply/order/getServiceList',
    method: 'POST',
    data,
  })
} //获取订单分页列表
