import request from '@/utils/request'
class LockService {

    getPageList(data: any) {
        return request({
            url: '/robot-api/admin/lock/pageList',
            method: 'POST',
            data,
        })
    }
    getList(data: any) {
        return request({
            url: '/robot-api/admin/lock/list',
            method: 'POST',
            data,
        })
    }
  
    addLock(data: any) {
        return request({
            url: '/robot-api/admin/lock/save',
            method: 'POST',
            data,
        })
    }
    updateLock(data: any) {
        return request({
            url: '/robot-api/admin/lock/update',
            method: 'POST',
            data,
        })
    }
    delLock(data: any) {
        return request({
            url: '/robot-api/admin/lock/delete',
            method: 'POST',
            data,
        })
    }
}
export const lockService = new LockService()