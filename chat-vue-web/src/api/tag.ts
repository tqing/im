import request from '@/utils/request'
/**
 *
 * @class TagService
 */

class TagService {
    /* 获取标签类型列表 */
    getCategoryList(data) {
        return request({
            url: '/robot-api/admin/tag/category/list',
            method: 'POST',
            data,
        })
    }
    /* 新增标签类型 */
    addCategory(data) {
        return request({
            url: '/robot-api/admin/tag/category/add',
            method: 'POST',
            data,
        })
    }
    /* 修改标签类型 */
    updateCategory(data) {
        return request({
            url: '/robot-api/admin/tag/category/update',
            method: 'POST',
            data,
        })
    }
    /* 删除标签类型 */
    delCategory(data) {
        return request({
            url: '/robot-api/admin/tag/category/del',
            method: 'POST',
            data,
        })
    }
    /* 查询标签分类及下级标签列表 */
    categoryAndTagTree(data) {
        return request({
            url: '/robot-api/admin/tag/category/categoryAndTagTree',
            method: 'POST',
            data,
        })
    }
    /* 获取标签列表 */
    getTagList(data) {
        return request({
            url: '/robot-api/admin/tag/pageList',
            method: 'POST',
            data,
        })
    }
    addTag(data) {
        return request({
            url: '/robot-api/admin/tag/add',
            method: 'POST',
            data,
        })
    }
    updateTag(data) {
        return request({
            url: '/robot-api/admin/tag/update',
            method: 'POST',
            data,
        })
    }
    delTag(data) {
        return request({
            url: '/robot-api/admin/tag/del',
            method: 'POST',
            data,
        })
    }
    addTagRelation(data) {
        return request({
            url: '/robot-api/admin/tag/relation/add',
            method: 'POST',
            data,
        })
    }
}

export const tagService = new TagService()
