import request from '@/utils/request';
/**
 *
 * @class basicConfigService
 */

class BasicConfigService {
  // 新增小区
  communityBatchSave(data) {
    return request({
      url: '/robot-api/admin/community/batchSave',
      method: 'POST',
      data,
    });
  }
  // 小区分页
  pageListCommunityResDto(data) {
    return request({
      url: '/robot-api/admin/community/pageListCommunityResDto',
      method: 'POST',
      data,
    });
  }
  // 小区分页
  getAllCommunity(data) {
    return request({
      url: '/robot-api/admin/community/getAllCommunity',
      method: 'POST',
      data,
    });
  }

  // 编辑小区
  communityUpdate(data) {
    return request({
      url: '/robot-api/admin/community/update',
      method: 'POST',
      data,
    });
  }
  // 删除小区
  communityBatchDel(data) {
    return request({
      url: '/robot-api/admin/community/batchDel',
      method: 'POST',
      data,
    });
  }
  // 小区详情
  communityGet(data) {
    return request({
      url: '/robot-api/admin/community/get',
      method: 'POST',
      data,
    });
  }

  // 台区列表
  areaGetList(data) {
    return request({
      url: '/robot-api/admin/area/getList',
      method: 'POST',
      data,
    });
  }
  // 新增台区
  areaBatchSave(data) {
    return request({
      url: '/robot-api/admin/area/batchSave',
      method: 'POST',
      data,
    });
  }
  // 台区分页
  pageListGridAreaResDto(data) {
    return request({
      url: '/robot-api/admin/area/pageListGridAreaResDto',
      method: 'POST',
      data,
    });
  }
  // 获取台区列表
  queryGridAreaDtoList(data) {
    return request({
      url: '/robot-api/admin/area/queryGridAreaDtoList',
      method: 'POST',
      data,
    });
  }
  //供电单位/线路/台区/级联查询
  getByAreaQueryDto(data) {
    return request({
      url: '/robot-api/admin/area/getByAreaQueryDto',
      method: 'POST',
      data,
    });
  }
  // 编辑台区
  areaUpdate(data) {
    return request({
      url: '/robot-api/admin/area/update',
      method: 'POST',
      data,
    });
  }
  // 删除台区
  areaBatchDel(data) {
    return request({
      url: '/robot-api/admin/area/batchDel',
      method: 'POST',
      data,
    });
  }
  // 台区详情
  areaGet(data) {
    return request({
      url: '/robot-api/admin/area/get',
      method: 'POST',
      data,
    });
  }

  // 网格经理分页
  pageListManagerResDto(data) {
    return request({
      url: '/robot-api/admin/manager/manage/pageListManagerResDto',
      method: 'POST',
      data,
    });
  }
  // 新增网格经理
  manageBatchSave(data) {
    return request({
      url: '/robot-api/admin/manager/manage/batchSave',
      method: 'POST',
      data,
    });
  }
  // 编辑网格经理
  manageUpdate(data) {
    return request({
      url: '/robot-api/admin/manager/manage/update',
      method: 'POST',
      data,
    });
  }
  // 删除网格经理
  manageBatchDel(data) {
    return request({
      url: '/robot-api/admin/manager/manage/deleteBatch',
      method: 'POST',
      data,
    });
  }

  // 网格机器人分页
  pageListRobotResDto(data) {
    return request({
      url: '/robot-api/admin/manager/robot/pageListRobotResDto',
      method: 'POST',
      data,
    });
  }
  // 新增网格机器人
  robotBatchSave(data) {
    return request({
      url: '/robot-api/admin/manager/robot/batchSave',
      method: 'POST',
      data,
    });
  }
  // 编辑网格机器人
  robotUpdate(data) {
    return request({
      url: '/robot-api/admin/manager/robot/update',
      method: 'POST',
      data,
    });
  }
  // 删除网格机器人
  robotBatchDel(data) {
    return request({
      url: '/robot-api/admin/manager/robot/deleteBatch',
      method: 'POST',
      data,
    });
  }
  // 启用网格机器人
  robotEnableBatch(data) {
    return request({
      url: '/robot-api/admin/manager/robot/enableBatch',
      method: 'POST',
      data,
    });
  }
  robotRefersh(data) {
    return request({
      url: '/robot-api/admin/manager/pushMqSyncPull',
      method: 'POST',
      data,
    });
  }
  robotChangePrivate (data) {
    return request({
      url: '/robot-api/admin/manager/switchPrivateChat',
      method: 'POST',
      data,
    });
  }
  // 停用网格机器人
  robotDisableBatch(data) {
    return request({
      url: '/robot-api/admin/manager/robot/disableBatch',
      method: 'POST',
      data,
    });
  }

  // 微信群分页
  pageListWxGroupResDto(data) {
    return request({
      url: '/robot-api/admin/group/pageListWxGroupResDto',
      method: 'POST',
      data,
    });
  }
  // 编辑微信群
  groupUpdate(data) {
    return request({
      url: '/robot-api/admin/group/update',
      method: 'POST',
      data,
    });
  }
  // 删除微信群
  groupBatchDel(data) {
    return request({
      url: '/robot-api/admin/group/deleteBatch',
      method: 'POST',
      data,
    });
  }
  // 启用微信群
  groupEnableBatch(data) {
    return request({
      url: '/robot-api/admin/group/enableBatch',
      method: 'POST',
      data,
    });
  }
  // 停用微信群
  groupDisableBatch(data) {
    return request({
      url: '/robot-api/admin/group/disableBatch',
      method: 'POST',
      data,
    });
  }

  // 查询所有网格经理
  getAllManager(data) {
    return request({
      url: '/robot-api/admin/manager/getAllManager',
      method: 'POST',
      data,
    });
  }

  // 停电信息启用
  powerEnableBatch(data) {
    return request({
      url: '/robot-api/admin/group/powerEnableBatch',
      method: 'POST',
      data,
    });
  }
  // 停电信息启用
  powerDisableBatch(data) {
    return request({
      url: '/robot-api/admin/group/powerDisableBatch',
      method: 'POST',
      data,
    });
  }
  // 根据机器人wxRobotId, 查询机器人状态信息
  getRobotInfoDtoByWxRobotId(data) {
    return request({
      url: '/robot-api/admin/robotInfo/getRobotInfoDtoByWxRobotId',
      method: 'POST',
      data,
    });
  }
  //启用自动回复
  keyWordEnableBatch(data) {
    return request({
      url: '/robot-api/admin/group/keyWordEnableBatch',
      method: 'POST',
      data,
    });
  }
  //禁用用自动回复
  keyWordDisableBatch(data) {
    return request({
      url: '/robot-api/admin/group/keyWordDisableBatch',
      method: 'POST',
      data,
    });
  }
  //获取机器人关键词回复配置
  getKeyWordConfig(data) {
    return request({
      url: '/robot-api/admin/robotKeyConfig/get',
      method: 'POST',
      data,
    });
  }
  //更新机器人关键词回复配置
  updateKeyWordConfig(data) {
    return request({
      url: '/robot-api/admin/robotKeyConfig/update',
      method: 'POST',
      data,
    });
  }
  //查询微信群消息发送限制
  getConfigInfo(data) {
    return request({
      url: '/robot-api/admin/group/message/send/number/config/info',
      method: 'POST',
      data,
    });
  }
  //编辑微信群消息发送限制
  configEdit(data) {
    return request({
      url: '/robot-api/admin/group/message/send/number/config/edit',
      method: 'POST',
      data,
    });
  }
  // 涉电名词
  electricPageList(data) {
    return request({
      url: '/robot-api/admin/electric/pageList',
      method: 'POST',
      data,
    });
  }
  // 新增涉电名词
  electricSave(data) {
    return request({
      url: '/robot-api/admin/electric/save',
      method: 'POST',
      data,
    });
  }
  // 更新涉电名词
  electricUpdate(data) {
    return request({
      url: '/robot-api/admin/electric/update',
      method: 'POST',
      data,
    });
  }
  // 更新涉电名词
  electricDelete(data) {
    return request({
      url: '/robot-api/admin/electric/delete',
      method: 'POST',
      data,
    });
  }
  // 涉电名词导出
  electricDownload(data) {
    return request({
      url: '/robot-api/admin/electric/pageList/download',
      method: 'POST',
      data,
      responseType: 'blob',
    });
  }
  // 批量添加管理员

  addAdmin(data) {
    return request({
      url: '/robot-api/admin/manager/staff/batchSave',
      method: 'POST',
      data,
    });
  }

  // 更新管理员信息
  updateAdmin(data) {
    return request({
      url: '/robot-api/admin/manager/staffManager/update',
      method: 'POST',
      data,
    });
  }
  // 获取所有管理员-下拉选择用
  getAllAdmin(data) {
    return request({
      url: '/robot-api/admin/manager/getAllStaffManager',
      method: 'POST',
      data,
    });
  }

  //  设为取消特殊人员
  setSpecialStatus(data) {
    return request({
      url: '/robot-api/admin/wx/user/changeSpecial',
      method: 'POST',
      data,
    });
  }

  // 机器人回复台账列表
  pageListRobotReply(data) {
    return request({
      url: '/robot-api/admin/wx/group/record/pageList',
      method: 'POST',
      data,
    });
  }

  pageListRobotReplyDownload(data) {
    return request({
      url: '/robot-api/admin/wx/group/record/download',
      method: 'POST',
      data,
      responseType: 'blob',
    });
  }
  managerReplDownload(data) {
    return request({
      url: '/robot-api/admin/wx/group/record/managerReplDownload',
      method: 'POST',
      data,
      responseType: 'blob',
    });
  }

  // 网格经理回复台账查看回复内容
  getRobotReply(data) {
    return request({
      url: '/robot-api/admin/wx/group/record/pageManagerAndRobotMessage',
      method: 'POST',
      data,
    });
  }
  // 诉求统计列表
  pageListAppeal(data) {
    return request({
      url: '/robot-api/admin/wx/group/record/pageRecordStatistics',
      method: 'POST',
      data,
    });
  }
  pageListAppealDownload(data) {
    return request({
      url: '/robot-api/admin/wx/group/record/pageRecordStatisticsDownload',
      method: 'POST',
      data,
      responseType: 'blob',
    });
  }
  // 网格经理私聊机器人台账消息详情
  pageListManagerAndRobotMessage(data) {
    return request({
      url: '/robot-api/admin/wx/group/record/pageManagerAndRobotMessage',
      method: 'POST',
      data,
    });
  }
  // 查询所有机器人-下拉列表
  getAllRobot(data) {
    return request({
      url: '/robot-api/admin/manager/getAllRobot',
      method: 'POST',
      data,
    });
  }
  // 微信用户导出
  wxUserDownload(data) {
    return request({
      url: '/robot-api/admin/wx/user/pageListWxGroupResDtoDownload',
      method: 'POST',
      data,
      responseType: 'blob',
    });
  }
  // 查询触发导出
  queryExport(data) {
    return request({
      url: '/robot-api/admin/opinionKeyRecord/detailDownload',
      method: 'POST',
      data,
      responseType: 'blob',
    });
  }
  // 系统日志导出
  systemLogDownload(data) {
    return request({
      url: '/robot-api/admin/sys/log/download',
      method: 'POST',
      data,
      responseType: 'blob',
    });
  }
}

export const basicConfigService = new BasicConfigService();
