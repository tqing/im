import request from '@/utils/request';
/**
 *
 * @class contentManagementService
 */

class ContentManagementService {
  // 停电信息分页
  pageListMessageResDto(data) {
    return request({
      url: '/robot-api/admin/message/pageListMessageResDto',
      method: 'POST',
      data,
    });
  }
  // 新增停电信息
  saveMessage(data) {
    return request({
      url: '/robot-api/admin/message/saveMessage',
      method: 'POST',
      data,
    });
  }

  // 常见问题分页
  libCommonPageList(data) {
    return request({
      url: '/robot-api/admin/libCommon/pageList',
      method: 'POST',
      data,
    });
  }
  // 常见问题删除
  libCommonDeleteBatch(data) {
    return request({
      url: '/robot-api/admin/libCommon/deleteBatch',
      method: 'POST',
      data,
    });
  }
  // 常见问题启用
  libCommonEnableBatch(data) {
    return request({
      url: '/robot-api/admin/libCommon/enableBatch',
      method: 'POST',
      data,
    });
  }
  // 常见问题停用
  libCommonDisableBatch(data) {
    return request({
      url: '/robot-api/admin/libCommon/disableBatch',
      method: 'POST',
      data,
    });
  }
  // 常见问题新增
  libCommonSaveBatch(data) {
    return request({
      url: '/robot-api/admin/libCommon/saveBatch',
      method: 'POST',
      data,
    });
  }
  // 常见问题编辑
  libCommonUpdate(data) {
    return request({
      url: '/robot-api/admin/libCommon/update',
      method: 'POST',
      data,
    });
  }
  libCommonUpdate2(data) {
    return request({
      url: '/robot-api/admin/libCommon/updateUnknown',
      method: 'POST',
      data,
    });
  }

  // 关键词分页
  libKeyPageList(data) {
    return request({
      url: '/robot-api/admin/libKey/pageList',
      method: 'POST',
      data,
    });
  }
  // 关键词删除
  libKeyDeleteBatch(data) {
    return request({
      url: '/robot-api/admin/libKey/deleteBatch',
      method: 'POST',
      data,
    });
  }
  // 关键词启用
  libKeyEnableBatch(data) {
    return request({
      url: '/robot-api/admin/libKey/enableBatch',
      method: 'POST',
      data,
    });
  }
  // 关键词停用
  libKeyDisableBatch(data) {
    return request({
      url: '/robot-api/admin/libKey/disableBatch',
      method: 'POST',
      data,
    });
  }
  // 关键词新增
  libKeySaveBatch(data) {
    return request({
      url: '/robot-api/admin/libKey/saveBatch',
      method: 'POST',
      data,
    });
  }
  // 关键词编辑
  libKeyUpdate(data) {
    return request({
      url: '/robot-api/admin/libKey/update',
      method: 'POST',
      data,
    });
  }
  // 关键词编辑2
  libKeyUpdate2(data) {
    return request({
      url: '/robot-api/admin/libKey/updateUnknown',
      method: 'POST',
      data,
    });
  }

  // 敏感词分页
  libSensitivePageList(data) {
    return request({
      url: '/robot-api/admin/libSensitive/pageList',
      method: 'POST',
      data,
    });
  }
  // 敏感词删除
  libSensitiveDeleteBatch(data) {
    return request({
      url: '/robot-api/admin/libSensitive/deleteBatch',
      method: 'POST',
      data,
    });
  }
  // 敏感词启用
  libSensitiveEnableBatch(data) {
    return request({
      url: '/robot-api/admin/libSensitive/enableBatch',
      method: 'POST',
      data,
    });
  }
  // 敏感词停用
  libSensitiveDisableBatch(data) {
    return request({
      url: '/robot-api/admin/libSensitive/disableBatch',
      method: 'POST',
      data,
    });
  }
  // 敏感词新增
  libSensitiveSaveBatch(data) {
    return request({
      url: '/robot-api/admin/libSensitive/saveBatch',
      method: 'POST',
      data,
    });
  }
  // 敏感词编辑
  libSensitiveUpdate(data) {
    return request({
      url: '/robot-api/admin/libSensitive/update',
      method: 'POST',
      data,
    });
  }

  // 舆情关键词分页
  libOpinionKeyPageList(data) {
    return request({
      url: '/robot-api/admin/libOpinionKey/pageList',
      method: 'POST',
      data,
    });
  }
  // 舆情关键词删除
  libOpinionKeyDeleteBatch(data) {
    return request({
      url: '/robot-api/admin/libOpinionKey/deleteBatch',
      method: 'POST',
      data,
    });
  }
  // 舆情关键词启用
  libOpinionKeyEnableBatch(data) {
    return request({
      url: '/robot-api/admin/libOpinionKey/enableBatch',
      method: 'POST',
      data,
    });
  }
  // 舆情关键词停用
  libOpinionKeyDisableBatch(data) {
    return request({
      url: '/robot-api/admin/libOpinionKey/disableBatch',
      method: 'POST',
      data,
    });
  }
  // 舆情关键词新增
  libOpinionKeySaveBatch(data) {
    return request({
      url: '/robot-api/admin/libOpinionKey/saveBatch',
      method: 'POST',
      data,
    });
  }
  // 舆情关键词编辑
  libOpinionKeyUpdate(data) {
    return request({
      url: '/robot-api/admin/libOpinionKey/update',
      method: 'POST',
      data,
    });
  }
  // 未知问题
  getUnknownCommon(data) {
    return request({
      url: '/robot-api/admin/libCommon/getUnknownCommon',
      method: 'POST',
      data,
    });
  }
  // 消息群发送消息记录详情,分页列表查询
  messageGroupRecordPageList(data) {
    return request({
      url: '/robot-api/admin/messageGroupRecord/pageList',
      method: 'POST',
      data,
    });
  }
  // 数据统计
  messageGroupRecordGetCount(data) {
    return request({
      url: '/robot-api/admin/messageGroupRecord/getCount',
      method: 'POST',
      data,
    });
  }
  // 停电信息查询
  messageGet(data) {
    return request({
      url: '/robot-api/admin/message/get',
      method: 'POST',
      data,
    });
  }
  // 添加信息发布
  publishMessageAdd(data) {
    return request({
      url: '/robot-api/admin/publish/message/add',
      method: 'POST',
      data,
    });
  }
  // 信息发布列表
  publishMessagePageList(data) {
    return request({
      url: '/robot-api/admin/publish/message/pageList',
      method: 'POST',
      data,
    });
  }
  // 信息发布详情
  publishMessageDetail(data) {
    return request({
      url: '/robot-api/admin/publish/message/detail',
      method: 'POST',
      data,
    });
  }
  // 信息发布发送好友分页列表
  relationPageUserList(data) {
    return request({
      url: '/robot-api/admin/publish/message/user/relation/pageUserList',
      method: 'POST',
      data,
    });
  }
  // 信息发布微信群分页列表
  relationPageGroupList(data) {
    return request({
      url: '/robot-api/admin/publish/message/user/relation/pageGroupList',
      method: 'POST',
      data,
    });
  }
  // 消息发布群聊批量审核
  relationBatchAudit(data) {
    return request({
      url: '/robot-api/admin/publish/message/user/relation/batchAudit',
      method: 'POST',
      data,
    });
  }
  // 消息发布发送好友统计
  relationWxUserStatistics(data) {
    return request({
      url: '/robot-api/admin/publish/message/user/relation/wxUserStatistics',
      method: 'POST',
      data,
    });
  }
  // 信息发布微信群信息发布统计
  relationWxGroupStatisticsResDto(data) {
    return request({
      url: '/robot-api/admin/publish/message/user/relation/wxGroupStatisticsResDto',
      method: 'POST',
      data,
    });
  }
  //未知关键词,查询
  getUnknownKey(data) {
    return request({
      url: '/robot-api/admin/libKey/getUnknownKey',
      method: 'POST',
      data,
    });
  }
  /* 停电信息导入 */
  importMessage(data) {
    return request({
      url: '/robot-api/admin/message/import',
      method: 'POST',
      data,
    });
  }
  /* 导出失败 */
  failMessageDownload(data) {
    return request({
      url: '/robot-api/admin/message/failMessageDownload',
      method: 'POST',
      data,
      responseType: 'blob',
    });
  }
}

export const contentManagementService = new ContentManagementService();
