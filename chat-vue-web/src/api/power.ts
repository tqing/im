import request from '@/utils/request';
class PowerService {
  getPowerPageList(data: any) {
    return request({
      url: '/robot-api/admin/power/pageList',
      method: 'POST',
      data,
    });
  }
  getPowerList(data: any) {
    return request({
      url: '/robot-api/admin/area/getByAreaQueryByPower',
      method: 'POST',
      data,
    });
  }
  addPower(data: any) {
    return request({
      url: '/robot-api/admin/power/save',
      method: 'POST',
      data,
    });
  }
  updatePower(data: any) {
    return request({
      url: '/robot-api/admin/power/update',
      method: 'POST',
      data,
    });
  }
  delPower(data: any) {
    return request({
      url: '/robot-api/admin/power/batchDel',
      method: 'POST',
      data,
    });
  }
  queryByQueryDto(data: any) {
    return request({
      url: '/robot-api/admin/power/queryByQueryDto',
      method: 'POST',
      data,
    });
  }
}
export const powerService = new PowerService();
