import request from '@/utils/request'
class LibraryService {

    getPageList(data: any) {
        return request({
            url: '/robot-api/admin/material/library/pageList',
            method: 'POST',
            data,
        })
    }
 
    add(data: any) {
        return request({
            url: '/robot-api/admin/material/library/insert',
            method: 'POST',
            data,
        })
    }
    update(data: any) {
        return request({
            url: '/robot-api/admin/material/library/update',
            method: 'POST',
            data,
        })
    }
    delete(data: any) {
        return request({
            url: '/robot-api/admin/material/library/del',
            method: 'POST',
            data,
        })
    }
    changeStatus(data: any) {
        return request({
            url: '/robot-api/admin/material/library/changeStatus',
            method: 'POST',
            data,
        })
    }
}
export const libraryService = new LibraryService()