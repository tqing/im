import request from '@/utils/request'
class LibSensitiveService {

    getList(data: any) {
        return request({
            url: '/robot-api/admin/libSensitive/category/list',
            method: 'POST',
            data,
        })
    }
 
    add(data: any) {
        return request({
            url: '/robot-api/admin/libSensitive/category/add',
            method: 'POST',
            data,
        })
    }
    update(data: any) {
        return request({
            url: '/robot-api/admin/libSensitive/category/update',
            method: 'POST',
            data,
        })
    }
    delete(data: any) {
        return request({
            url: '/robot-api/admin/libSensitive/category/del',
            method: 'POST',
            data,
        })
    }
    
}
export const libSensitiveService = new LibSensitiveService()