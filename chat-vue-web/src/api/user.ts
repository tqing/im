import request from '@/utils/request'
class UserService {

    getList(data: any) {
        return request({
            url: '/tenant-api/admin/tenant/user/userList',
            method: 'GET',
            
        })
    }
 
   
}
export const userService = new UserService()