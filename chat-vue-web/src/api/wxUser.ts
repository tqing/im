import request from '@/utils/request'
/**
 *
 * @class WxUserService
 */

class WxUserService {
    /* 微信用户管理, 分页查询 */
    getWxUserList(data) {
        return request({
            url: '/robot-api/admin/wx/user/pageListWxGroupResDto',
            method: 'POST',
            data,
        })
    }
    editWxUser(data) {
        return request({
            url: '/robot-api/admin/wx/user/edit',
            method: 'POST',
            data,
        })
    }
    getCutomerUserList(data) {
        return request({
            url: '/robot-api/admin/customer/pageListWxGroupResDto',
            method: 'POST',
            data,
        })
    }
    robotAndWxUserList(data) {
        return request({
          url: '/robot-api/admin/wx/user/robotAndWxUserList',
          method: 'POST',
          data,
        })
      }
   
}

export const wxUserService = new WxUserService()
