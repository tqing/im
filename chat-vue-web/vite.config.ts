import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import viteCompression from 'vite-plugin-compression'
import path from 'path'
import qiankun from 'vite-plugin-qiankun'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
// import { visualizer } from 'rollup-plugin-visualizer';
// import externalGlobals from 'rollup-plugin-external-globals'
const pathSrc = path.resolve(__dirname, 'src')
export default defineConfig(({ command, mode }) => {
  return {
    server: {
      hmr: false,
      host: '0.0.0.0',
      port: 9091,
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      // proxy: {
      //   '/api': {
      //     target: '',
      //     changeOrigin: true,
      //     rewrite: (path) => path.replace(/^\/api/, ''),
      //   },
      // },
      proxy: {
        '/upload-api': {
          target:  `${loadEnv(mode, process.cwd()).VITE_CONFIG_FILE}/put`,
          changeOrigin: true,
          ws: true,
          rewrite: (path) => path.replace(/^\/upload-api/, ''),
         
        },
        '/down-api': {
          target:  `${loadEnv(mode, process.cwd()).VITE_CONFIG_FILE}/get`,
          changeOrigin: true,
          ws: true,
          rewrite: (path) => path.replace(/^\/down-api/, ''),
         
        },

      },
    },
    plugins: [
      vue(),
      qiankun('robot', {
        // 微应用名字，与主应用注册的微应用名字保持一致
        useDevMode: true,
      }),

      // visualizer({ open: true }),
      // vue自动导入
      AutoImport({
        imports: ['vue', 'vue-router'],
        resolvers: [ElementPlusResolver()],
        dts: path.resolve(pathSrc, 'auto-imports.d.ts'),
      }),
      Components({
        resolvers: [ElementPlusResolver()],
        dts: path.resolve(pathSrc, 'components.d.ts'),
      }),
      // 压缩
      viteCompression({
        verbose: true, // 默认即可
        disable: false, //开启压缩(不停用)，默认即可
        deleteOriginFile: false, //删除源文件
        threshold: 1024, //压缩前最小文件大小
        algorithm: 'gzip', //压缩算法
      }),
    ],
    define: {
      'process.env': loadEnv(mode, process.cwd()),
    },
    resolve: {
      // 配置路径别名
      alias: {
        // 如果报错__dirname找不到，需要安装node,执行npm install @types/node --save-dev
        '@': path.resolve(__dirname, 'src'),
        '~/': `${path.resolve(__dirname, 'src')}/`,
        '@assets': path.resolve(__dirname, './src/assets'),
        '@components': path.resolve(__dirname, './src/components'),
        '@views': path.resolve(__dirname, './src/views'),
        '@store': path.resolve(__dirname, './src/store'),
      },
    },
    css: {
      preprocessorOptions: {
        // 导入scss预编译程序
        scss: {
          additionalData: `@use "@/assets/style/common.scss" as *;`,
        },
      },
    },
    base: loadEnv(mode, process.cwd()).VITE_CONFIG_BASE,
    // base: '/index/robot',
    build: {
      outDir: 'dist', // 指定输出路径
      rollupOptions: {
        input: {
          index: path.resolve(__dirname, 'index.html'),
        },
        output: {
          chunkFileNames: 'static/js/[name]-[hash].js',
          entryFileNames: 'static/js/[name]-[hash].js',
          assetFileNames: 'static/[ext]/name-[hash].[ext]',
        },
      },
    },
  }
})
