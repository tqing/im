let api = [];
api.push({
    alias: 'api',
    order: '1',
    desc: '导航设置',
    link: '导航设置',
    list: []
})
api[0].list.push({
    order: '1',
    desc: '',
});
api[0].list.push({
    order: '2',
    desc: '',
});
api.push({
    alias: 'SessionController',
    order: '2',
    desc: 'Session 会话列表',
    link: 'session_会话列表',
    list: []
})
api[1].list.push({
    order: '1',
    desc: '',
});
api.push({
    alias: 'APNsController',
    order: '3',
    desc: 'APNs推送相关',
    link: 'apns推送相关',
    list: []
})
api[2].list.push({
    order: '1',
    desc: '',
});
api[2].list.push({
    order: '2',
    desc: '',
});
api.push({
    alias: 'WebrtcController',
    order: '4',
    desc: '单人通话信令推送接口',
    link: '单人通话信令推送接口',
    list: []
})
api[3].list.push({
    order: '1',
    desc: '',
});
api[3].list.push({
    order: '2',
    desc: '',
});
api[3].list.push({
    order: '3',
    desc: '',
});
api[3].list.push({
    order: '4',
    desc: '',
});
api[3].list.push({
    order: '5',
    desc: '',
});
api[3].list.push({
    order: '6',
    desc: '',
});
api[3].list.push({
    order: '7',
    desc: '',
});
api[3].list.push({
    order: '8',
    desc: '',
});
api[3].list.push({
    order: '9',
    desc: '',
});
api[3].list.push({
    order: '10',
    desc: '',
});
api.push({
    alias: 'MessageController',
    order: '5',
    desc: '消息相关接口',
    link: '消息相关接口',
    list: []
})
api[4].list.push({
    order: '1',
    desc: '发送消息',
});
api.push({
    alias: 'UserController',
    order: '6',
    desc: '用户登录接口',
    link: '用户登录接口',
    list: []
})
api[5].list.push({
    order: '1',
    desc: '登录',
});
api[5].list.push({
    order: '2',
    desc: '退出',
});
api[5].list.push({
    order: '3',
    desc: '注册账号',
});
document.onkeydown = keyDownSearch;
function keyDownSearch(e) {
    const theEvent = e;
    const code = theEvent.keyCode || theEvent.which || theEvent.charCode;
    if (code === 13) {
        const search = document.getElementById('search');
        const searchValue = search.value;
        let searchArr = [];
        for (let i = 0; i < api.length; i++) {
            let apiData = api[i];
            const desc = apiData.desc;
            if (desc.toLocaleLowerCase().indexOf(searchValue) > -1) {
                searchArr.push({
                    order: apiData.order,
                    desc: apiData.desc,
                    link: apiData.link,
                    alias: apiData.alias,
                    list: apiData.list
                });
            } else {
                let methodList = apiData.list || [];
                let methodListTemp = [];
                for (let j = 0; j < methodList.length; j++) {
                    const methodData = methodList[j];
                    const methodDesc = methodData.desc;
                    if (methodDesc.toLocaleLowerCase().indexOf(searchValue) > -1) {
                        methodListTemp.push(methodData);
                        break;
                    }
                }
                if (methodListTemp.length > 0) {
                    const data = {
                        order: apiData.order,
                        desc: apiData.desc,
                        alias: apiData.alias,
                        link: apiData.link,
                        list: methodListTemp
                    };
                    searchArr.push(data);
                }
            }
        }
        let html;
        if (searchValue === '') {
            const liClass = "";
            const display = "display: none";
            html = buildAccordion(api,liClass,display);
            document.getElementById('accordion').innerHTML = html;
        } else {
            const liClass = "open";
            const display = "display: block";
            html = buildAccordion(searchArr,liClass,display);
            document.getElementById('accordion').innerHTML = html;
        }
        const Accordion = function (el, multiple) {
            this.el = el || {};
            this.multiple = multiple || false;
            const links = this.el.find('.dd');
            links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown);
        };
        Accordion.prototype.dropdown = function (e) {
            const $el = e.data.el;
            let $this = $(this), $next = $this.next();
            $next.slideToggle();
            $this.parent().toggleClass('open');
            if (!e.data.multiple) {
                $el.find('.submenu').not($next).slideUp("20").parent().removeClass('open');
            }
        };
        new Accordion($('#accordion'), false);
    }
}

function buildAccordion(apiData, liClass, display) {
    let html = "";
    if (apiData.length > 0) {
         for (let j = 0; j < apiData.length; j++) {
            html += '<li class="'+liClass+'">';
            html += '<a class="dd" href="' + apiData[j].alias + '.html#header">' + apiData[j].order + '.&nbsp;' + apiData[j].desc + '</a>';
            html += '<ul class="sectlevel2" style="'+display+'">';
            let doc = apiData[j].list;
            for (let m = 0; m < doc.length; m++) {
                html += '<li><a href="' + apiData[j].alias + '.html#_' + apiData[j].order + '_' + doc[m].order + '_' + doc[m].desc + '">' + apiData[j].order + '.' + doc[m].order + '.&nbsp;' + doc[m].desc + '</a> </li>';
            }
            html += '</ul>';
            html += '</li>';
        }
    }
    return html;
}