package com.itanqing.nettydemo.client;


import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * BaseClient1Handler
 *
 * @author tanqing
 * @date 2023/12/18 17:31
 */
public class BaseClient1Handler  extends ChannelInboundHandlerAdapter {

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        //每一个handler只需要关注自己要处理的方法，如果你不关注channelActive方法时，你自定义的channelhandler就不需要重写channelActive方法

        System.out.println("BaseClient1Handler channelActive");
        ctx.fireChannelActive();
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {

        //异常处理，如果 exceptionCaught方法每个handler都重写了，只需有一个类捕捉到然后做处理就可以了，不需要每个handler都处理一遍
        System.out.println("BaseClient1Handler channelInactive");

    }
}
