package com.itanqing.nettydemo.client;


import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * BaseClient2Handler
 *
 * @author tanqing
 * @date 2023/12/18 17:32
 */
public class BaseClient2Handler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("BaseClient2Handler Active");
    }
}
