package com.itanqing.nettydemo.client;


import com.itanqing.nettydemo.proto.CustomMsg;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class CustomClientHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        String sendMessage ="Hello,Netty  i has a  test";
        CustomMsg customMsg = new CustomMsg((byte)0xAB, (byte)0xCD,sendMessage.length(), sendMessage);
        ctx.writeAndFlush(customMsg);
    }
}
