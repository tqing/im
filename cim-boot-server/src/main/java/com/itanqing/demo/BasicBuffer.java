package com.itanqing.demo;



import java.nio.IntBuffer;

/**
 * BasicBuffer
 * NIO  channel 通道， buffer 缓冲区 selector 悬着器  同步非阻塞网络模型
 *
 * NIO 和 BIO 的 比较
 * BIO以流的方式处理数据,而NIO以块的方式处理数据,块 I/O的效率比流 I/O高很多
 * BIO 是阻塞的, NIO则是非阻塞的
 * BIO基于字节流和字符流进行操作,而NIO基于Channel(通道)和Buffer(缓冲区)进行操作,数据总是从通道读取到缓冲区中,或者从缓冲区写入通道,Selector(选择器)用于监听多个通道的事件(比如L连接请求,数据到达等), 因此使用单个线程就可以监听多个客户端通道
 *
 * @author tanqing
 * @date 2023/12/7 18:33
 */
public class BasicBuffer {
    public static void main(String[] args) {

        // 新建Buffer
        // 5 容量
        IntBuffer intBuffer = IntBuffer.allocate(5);

        // 向Buffer中存放数据
        for (int i = 0; i < intBuffer.capacity(); i++) {
            IntBuffer put = intBuffer.put(i * 2);
        }

        // 从Buffer中获取数据
        // flip() : 将Buffer转换, 读写切换
        intBuffer.flip();

        // 获取数据
        while (intBuffer.hasRemaining()) {
            System.out.println(intBuffer.get());
        }
    }

}
