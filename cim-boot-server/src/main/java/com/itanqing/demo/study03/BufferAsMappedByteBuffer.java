package com.itanqing.demo.study03;


import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/**
 * NIO还提供了MappedByteBuffer, 可以让文件直接在内存(堆外的内存)中进行修改, 而如何同步到文件由NIO来完成
 *
 * 将文件02.txt中的Flower修改为Dance
 *
 */
public class BufferAsMappedByteBuffer {

    public static void main(String[] args) throws Exception {

        // 加载文件 权限为 rw
        RandomAccessFile randomAccessFile = new RandomAccessFile("src/main/resources/02.txt", "rw");

        FileChannel channel = randomAccessFile.getChannel();

        /*
         * FileChannel.MapMode.READ_WRITE : 模式
         * 6: 开始下标
         * 6: 从开始下标后的,修改字符数量
         */
        MappedByteBuffer map = channel.map(FileChannel.MapMode.READ_WRITE, 6, 6);

        map.put(0, (byte) 'D');
        map.put(1, (byte) 'a');
        map.put(2, (byte) 'n');
        map.put(3, (byte) 'c');
        map.put(4, (byte) 'e');
        map.put(5, (byte) ' ');

        channel.close();
        randomAccessFile.close();
        System.out.println("修改完成~~");
    }
}
