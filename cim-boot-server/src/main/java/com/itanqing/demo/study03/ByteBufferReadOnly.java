package com.itanqing.demo.study03;


import java.nio.ByteBuffer;

/**
 * 可以将一个普通Buffer转成只读Buffer
 */
public class ByteBufferReadOnly {
    public static void main(String[] args) {
        ByteBuffer buffer = ByteBuffer.allocate(64);
        // 写入数据
        for (byte i = 0; i < 64; i++) {
            buffer.put(i);
        }

        // 读写切换 换为只读
        buffer.flip();
        ByteBuffer readOnlyBuffer = buffer.asReadOnlyBuffer();

        while (readOnlyBuffer.hasRemaining()) {
            System.out.println(readOnlyBuffer.get());
        }

        // 尝试只读Buffer写入
        // 只读 读写切换
        readOnlyBuffer.flip();

        readOnlyBuffer.put((byte) 1);

    }

}