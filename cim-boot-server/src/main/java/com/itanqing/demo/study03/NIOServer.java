package com.itanqing.demo.study03;


import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/**
 * 编写一个NIO入门案例, 实现服务器和客户端之间的数据简单通讯(非阻塞)
 */
public class NIOServer {

    public static void main(String[] args) throws Exception {

        // 创建ServerSocketChannel
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();

        // 创建一个Selector
        Selector selector = Selector.open();

        // 绑定6666端口
        serverSocketChannel.socket().bind(new InetSocketAddress(6666));

        // 设置为非阻塞
        serverSocketChannel.configureBlocking(false);

        // 将ServerSocketChannel注册到Selector, 事件为 OP_ACCEPT
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);

        int opRead = SelectionKey.OP_READ; //1 << 0     1     代表读操作
        int opWrite = SelectionKey.OP_WRITE;//1 << 2    4     代表写操作
        int opConnect = SelectionKey.OP_CONNECT;// 1<<3  8    代表已建立连接
        int opAccept = SelectionKey.OP_ACCEPT; // 1<<4    16  有新的网络可以accept

//        位移运算
//        1  对应的二级制位 0001   左移动0位 还是0001 也就是还是1
//                              左移动2位     0100 也就是4
        //                      左移动3位     1000  8
        //                      左移动4位    0001 0000  16
        // 循环等待客户端连接
        for (int i = 3; ; i += 3) {

            // 判断是否等待1秒后 无事件
            if (selector.select(3000) == 0) {
                System.out.println("等待" + i + "秒后 , 无连接");
                continue;
            }

            /*
             *  1. 如果返回大于0 代表已经获取到关注的事件 就获取相关的SelectionKey 集合
             *  2. 调用selector.selectedKeys() 返回关注事件的集合
             *  3. 通过SelectionKey反向获取Channel
             *  4. 通过Channel处理数据
             */
            Set<SelectionKey> selectionKeys = selector.selectedKeys();

            Iterator<SelectionKey> iterator = selectionKeys.iterator();


            while (iterator.hasNext()) {
                SelectionKey selectionKey = iterator.next();
                // 如果是连接事件
                if (selectionKey.isAcceptable()) {
                    // 因为是事件驱动的, 所以已经判断是连接事件了 在这里调用accept不会阻塞, 会立即给要连接的客户端创建一个SocketChannel
                    SocketChannel socketChannel = serverSocketChannel.accept();
                    // 将SocketChannel设置为非阻塞
                    socketChannel.configureBlocking(false);
                    // 将SocketChannel注册到Selector, 并绑定读取事件, 指定字节Buffer的大小为1024
                    SelectionKey register = socketChannel.register(selector, SelectionKey.OP_READ, ByteBuffer.allocate(1024));
                    System.out.println("selection key is " + register);
                }

                // 如果是读取事件
                if (selectionKey.isReadable()) {
                    // 通过SelectionKey 获取Channel
                    SocketChannel socketChannel = (SocketChannel) selectionKey.channel();
                    // 通过SelectionKey 获取ByteBuffer
                    ByteBuffer buffer = (ByteBuffer) selectionKey.attachment();
                    // 将Channel的数据写入Buffer
                    socketChannel.read(buffer);

                    // 写完之后 切换为读 不然会直接读取到长度
                    buffer.flip();

                    // 不能直接使用返回的底层数组,应为会有很多空格, 需要设置偏移量和截止位置
                    System.out.println("from 客户端: " + new String(buffer.array(), 0, buffer.limit()));
                }

                // 从集合中移除Key防止重复处理
                iterator.remove();
            }
        }
    }
}
