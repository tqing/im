package com.itanqing.demo.study05;


import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

/**
 * 零拷贝
 */
public class ZeroCopyServer {

    public static void main(String[] args) throws Exception {

        InetSocketAddress inetSocketAddress = new InetSocketAddress(7001);

        ServerSocketChannel open = ServerSocketChannel.open();

        open.socket().bind(inetSocketAddress);

        for (; ; ) {
            SocketChannel accept = open.accept();
            int countSize = 0;
            ByteBuffer allocate = ByteBuffer.allocate(4096);
            while (-1 != countSize) {
                countSize = accept.read(allocate);
                allocate.rewind(); // 倒置 position=0 Mark 作废
            }
        }

    }

}

