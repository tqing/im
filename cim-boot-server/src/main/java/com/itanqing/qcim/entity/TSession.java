package com.itanqing.qcim.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author zs
 * @since 2023-12-14
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("t_session")
public class TSession implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String appVersion;

    private Long bindTime;

    private String channel;

    private String deviceId;

    private String deviceName;

    private String host;

    private String language;

    private Double latitude;

    private String location;

    private Double longitude;

    private String nid;

    private String osVersion;

    private Integer state;

    private String uid;

    /**
     * 用户id
     */
    private Long userId;


}
