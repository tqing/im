package com.itanqing.qcim.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author zs
 * @since 2023-12-14
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("t_account")
public class TAccount implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    private Long accountId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 是否删除
     */
    private Integer isDelete;

    /**
     * 账号名称
     */
    private String accountName;

    /**
     * 账户类型
     */
    private Integer accountType;

    /**
     * 密码
     */
    private String password;

    /**
     * 密码盐
     */
    private String salt;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
