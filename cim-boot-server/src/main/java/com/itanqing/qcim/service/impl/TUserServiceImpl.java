package com.itanqing.qcim.service.impl;

import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.itanqing.qcim.entity.TUser;
import com.itanqing.qcim.mapper.TUserMapper;
import com.itanqing.qcim.service.ITUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaohuoren.dubbo.common.exception.AppException;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zs
 * @since 2023-12-14
 */
@Service
public class TUserServiceImpl extends ServiceImpl<TUserMapper, TUser> implements ITUserService {

    @Override
    public TUser getByPhone(String phone) {
        return checkPhoneNumber(null,phone);
    }

    @Override
    public Long add(String phone, String userName) {
        TUser user = new TUser();
        user.setPhone(phone);
        user.setUserName(userName);

        TUser checkUser = checkPhoneNumber(null, user.getPhone());
        //如果手机号存在，直接返回userId
        if (!ObjectUtils.isEmpty(checkUser)) {
            return checkUser.getUserId();
        }
        //保存用户信息
        if (!save(user)) {
            throw new AppException("用户添加失败");
        }
        return user.getUserId();
    }

    private TUser checkPhoneNumber(Long userId, String phone) {
        return this.checkPhoneNumber(userId, phone);
    }
}
