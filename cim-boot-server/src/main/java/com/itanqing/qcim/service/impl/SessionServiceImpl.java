/*
 * Copyright 2013-2022 Xia Jun(3979434@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ***************************************************************************************
 *                                                                                     *
 *                        Website : http://www.farsunset.com                           *
 *                                                                                     *
 ***************************************************************************************
 */
package com.itanqing.qcim.service.impl;

import com.itanqing.qcim.component.redis.KeyValueRedisTemplate;
import com.itanqing.qcim.entity.Session;
import com.itanqing.qcim.repository.SessionRepository;
import com.itanqing.qcim.service.SessionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SessionServiceImpl implements SessionService {

    @Resource
    private SessionRepository sessionRepository;

    @Resource
    private KeyValueRedisTemplate keyValueRedisTemplate;


    private final String host;

    public SessionServiceImpl() throws UnknownHostException {
        host = InetAddress.getLocalHost().getHostAddress();
    }

    @Override
    public void add(Session session) {
        session.setBindTime(System.currentTimeMillis());
        session.setHost(host);
        sessionRepository.save(session);
    }

    @Override
    public void delete(long id) {
        sessionRepository.deleteById(id);
    }


    @Override
    public void deleteLocalhost() {
        sessionRepository.deleteAll(host);
    }

    @Override
    public void updateState(long id, int state) {
        sessionRepository.updateState(id,state);
    }

    @Override
    public void openApns(String uid,String deviceToken) {
        keyValueRedisTemplate.openApns(uid,deviceToken);
        sessionRepository.openApns(uid,Session.CHANNEL_IOS);
    }

    @Override
    public void closeApns(String uid) {
        keyValueRedisTemplate.closeApns(uid);
        sessionRepository.closeApns(uid,Session.CHANNEL_IOS);
    }

    @Override
    public List<Session> findAll() {
        return sessionRepository.findAll()
                .stream()
                .filter(session -> session.getState() == Session.STATE_ACTIVE || session.getState() == Session.STATE_APNS)
                .collect(Collectors.toList());
    }
}
