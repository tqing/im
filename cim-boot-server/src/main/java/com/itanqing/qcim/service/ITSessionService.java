package com.itanqing.qcim.service;

import com.itanqing.qcim.entity.TSession;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zs
 * @since 2023-12-14
 */
public interface ITSessionService extends IService<TSession> {

}
