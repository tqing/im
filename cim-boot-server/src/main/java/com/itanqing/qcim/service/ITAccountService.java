package com.itanqing.qcim.service;

import com.itanqing.qcim.dto.CodeLoginReqDto;
import com.itanqing.qcim.dto.LoginReqDto;
import com.itanqing.qcim.dto.RegisterReqDto;
import com.itanqing.qcim.dto.UserInfoResDto;
import com.itanqing.qcim.entity.TAccount;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zs
 * @since 2023-12-14
 */
public interface ITAccountService extends IService<TAccount> {

    public void register(RegisterReqDto registerReqDto);

    public UserInfoResDto login(LoginReqDto loginReqDto);

    public UserInfoResDto loginByCode(CodeLoginReqDto codeLoginReqDto);


    /**
     * 验证密码
     *
     * @param accountName
     * @param accountType
     * @param password
     * @return
     */
    TAccount checkPassword(String accountName, Integer accountType, String password);
}
