package com.itanqing.qcim.service;

import com.itanqing.qcim.entity.TUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zs
 * @since 2023-12-14
 */
public interface ITUserService extends IService<TUser> {
    public TUser getByPhone(String phone);
    public Long add(String phone,String  userName);
}
