package com.itanqing.qcim.service.impl;

import com.itanqing.qcim.entity.TSession;
import com.itanqing.qcim.mapper.TSessionMapper;
import com.itanqing.qcim.service.ITSessionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zs
 * @since 2023-12-14
 */
@Service
public class TSessionServiceImpl extends ServiceImpl<TSessionMapper, TSession> implements ITSessionService {

}
