/*
 * Copyright 2013-2019 Xia Jun(3979434@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ***************************************************************************************
 *                                                                                     *
 *                        Website : http://www.farsunset.com                           *
 *                                                                                     *
 ***************************************************************************************
 */
package com.itanqing.qcim.mvc.controller.api;


import com.itanqing.qcim.annotation.AccessToken;
import com.itanqing.qcim.dto.LoginReqDto;
import com.itanqing.qcim.dto.RegisterReqDto;
import com.itanqing.qcim.dto.UserInfoResDto;
import com.itanqing.qcim.mvc.response.ResponseEntity;
import com.itanqing.qcim.service.AccessTokenService;
import com.itanqing.qcim.service.ITAccountService;
import com.xiaohuoren.dubbo.common.bean.Request;
import com.xiaohuoren.dubbo.common.bean.Result;
//import io.swagger.annotations.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * 用户登录接口
 */
@RestController
@RequestMapping("/user")
//@Api(produces = "application/json", tags = "用户登录接口")
@Validated
public class UserController {

    @Resource
    private AccessTokenService accessTokenService;

    @Resource
    private ITAccountService itAccountService;

    /**
     * 登录
     * @param telephone
     * @return
     */
    @PostMapping(value = "/login")
    public ResponseEntity<?> login(@RequestParam String telephone) {


        Map<String, Object> body = new HashMap<>();
        body.put("id", Long.parseLong(telephone));
        body.put("name", "测试用户");
        body.put("telephone", "telephone");

        ResponseEntity<Map<String, Object>> result = new ResponseEntity<>();


        LoginReqDto loginReqDto = new LoginReqDto();
        loginReqDto.setLoginType(1);
        loginReqDto.setPassword("123456");
        loginReqDto.setAccountName(telephone);
        loginReqDto.setAccountType(1);

        UserInfoResDto login = itAccountService.login(loginReqDto);

        body.put("id", login.getUserId());
        body.put("name", login.getUserName());
        body.put("telephone", login.getAccountName());

        result.setData(body);

        result.setToken(accessTokenService.generate(telephone));
        result.setTimestamp(System.currentTimeMillis());
        return result;
    }


    /**
     * 退出
     * @param token
     * @return
     */
    @GetMapping(value = "/logout")
    public ResponseEntity<Void> logout(
//            @ApiParam(hidden = true)
            @AccessToken String token) {
        accessTokenService.delete(token);
        return ResponseEntity.make();
    }


    /**
     * 注册账号
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.POST)
    public Result<Void> register(@RequestBody Request<RegisterReqDto> request) {
        itAccountService.register(request.getData());
        return Result.<Void>create().success();
    }

}
