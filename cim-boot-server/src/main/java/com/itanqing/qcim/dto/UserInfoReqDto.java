package com.itanqing.qcim.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author tanqing
 * @title
 * @create 2022-08-31 15:22
 * @since
 */
@Data
public class UserInfoReqDto implements Serializable {

    /**
     * 用户ID
     */
    @NotNull(message = "用户ID不能为空")
    private Long userId;
}
