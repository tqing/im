package com.itanqing.qcim.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author tanqing
 * @title
 * @create 2022-08-29 14:15
 * @since
 */
@Data
public class CodeLoginReqDto implements Serializable {

    /**
     * 手机号码
     */
    @NotNull(message = "手机号码不能为空")
    private String phoneNumber;

    /**
     * 短信验证码
     */
    @NotNull(message = "短信验证码不能为空")
    private String code;

    /**
     * 邀请类型   1 用户邀请  2 门店邀请
     */
    private Integer inviteType;

    /**
     * 邀请人id
     */
    private Long inviteUserId;

    /**
     * 邀请人用户名称
     */
    private String inviteUserName;

    /**
     * 邀请门店id
     */
    private Long inviteStoreId;

    /**
     * 活动id
     */
    private Long activityId;

}
