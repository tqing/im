package com.itanqing.qcim.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author tanqing
 * @title
 * @create 2022-08-29 14:15
 * @since
 */
@Data
public class LoginReqDto implements Serializable {
    /**
     * 账号
     */
    private String accountName;

    /**
     * 密码
     */
    private String password;

    /**
     * 账户类型  1后台 2门店 3客户(默认)
     */
    private Integer accountType = 3;

    /**
     * 登录类型  1账号密码(默认) 2短信验证码
     */
    private Integer loginType = 1;

}
