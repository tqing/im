package com.itanqing.qcim.dto;

import com.xiaohuoren.dubbo.common.bean.LoginResV2Dto;
import lombok.Data;

import java.io.Serializable;

/**
 * @author tanqing
 * @title
 * @create 2022-08-31 15:22
 * @since
 */
@Data
public class UserInfoResDto extends LoginResV2Dto implements Serializable {

    /**
     * 头像地址
     */
    private String avatar;
}
