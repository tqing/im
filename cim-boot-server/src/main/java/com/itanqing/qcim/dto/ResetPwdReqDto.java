package com.itanqing.qcim.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author tanqing
 * @title
 * @create 2022-09-01 11:03
 * @since
 */
@Data
public class ResetPwdReqDto  implements Serializable {

    private Long  id;

    private String password;
}
