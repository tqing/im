package com.itanqing.qcim.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author tanqing
 * @title
 * @create 2022-08-29 14:18
 * @since
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterReqDto extends InviteReqDto {

    /**
     * 账号
     */
    private String accountName;

    /**
     * 密码
     */
    private String password;

    /**
     * 账户类型  1后台 2门店 3客户(默认)
     */
    private Integer accountType;

    /**
     * 业务id
     */
    private Long bizId;

    /**
     * 是否门店管理员：1是0否
     */
    private Integer isManager = 0;

    /**
     * 用户昵称（注册时后台生成）
     */
    private String userName;

}
