package com.itanqing.qcim.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author tanqing
 * @title
 * @create 2022-08-29 14:15
 * @since
 */
@Data
public class GetCodeReqDto implements Serializable {

    /**
     * 手机号码
     */
    @NotNull(message = "手机号码不能为空")
    private String phoneNumber;

}
