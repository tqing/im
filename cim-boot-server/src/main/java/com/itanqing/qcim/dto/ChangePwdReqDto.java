package com.itanqing.qcim.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author tanqing
 * @title
 * @create 2022-08-30 15:14
 * @since
 */
@Data
public class ChangePwdReqDto implements Serializable {

    /**
     * 原密码
     */
    private String oldPassword;

    /**
     * 新密码
     */
    private String newPassword;
}
