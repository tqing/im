package com.itanqing.qcim.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author tanqing
 * @title
 * @create 2022-08-29 14:15
 * @since
 */
@Data
public class LoginResDto implements Serializable {

    private String loginToken;
    private Long userId;
    private Long accountId;
    private Integer accountType;
}
