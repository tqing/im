package com.itanqing.qcim.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author tanqing
 * @title
 * @create 2022-08-29 14:18
 * @since
 */
@Data
public class InviteReqDto implements Serializable {

    /**
     * 邀请类型   1 用户邀请  2 门店邀请
     */
    private Integer inviteType;

    /**
     * 邀请人id
     */
    private Long inviteUserId;

    /**
     * 邀请人用户名称
     */
    private String inviteUserName;

    /**
     * 邀请门店id
     */
    private Long inviteStoreId;

    /**
     * 活动id
     */
    private Long activityId;

}
