package com.itanqing.qcim.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itanqing.qcim.entity.TAccount;
import com.xiaohuoren.dubbo.common.enums.IsDeleteEnum;
import com.xiaohuoren.dubbo.mybatis.extmapper.BaseExtMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zs
 * @since 2023-12-14
 */
public interface TAccountMapper extends BaseExtMapper<TAccount> {

    /**
     * 写入数据
     *
     * @param tAccount
     * @return
     */
    default TAccount insertData(TAccount tAccount) {
        this.insert(tAccount);
        return tAccount;
    }

    /**
     * 修改数据
     *
     * @param tAccount
     * @return
     */
    default TAccount updateData(TAccount tAccount) {
        this.updateById(tAccount);
        return tAccount;
    }

    default TAccount  getAccount(String accountName){
        QueryWrapper<TAccount> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(TAccount::getAccountName,accountName).eq(TAccount::getIsDelete, IsDeleteEnum.NO.getCode());
        List<TAccount> tAccounts = this.selectList(wrapper);
        if (tAccounts!=null  && tAccounts.size()>0){
            return  tAccounts.get(0);
        }
        return  null;
    }

}
