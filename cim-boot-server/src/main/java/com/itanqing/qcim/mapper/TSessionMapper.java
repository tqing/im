package com.itanqing.qcim.mapper;

import com.itanqing.qcim.entity.TSession;
import com.xiaohuoren.dubbo.mybatis.extmapper.BaseExtMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zs
 * @since 2023-12-14
 */
public interface TSessionMapper extends BaseExtMapper<TSession> {

}
