package com.itanqing.qcim.mapper;

import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.itanqing.qcim.entity.TUser;
import com.xiaohuoren.dubbo.mybatis.extmapper.BaseExtMapper;
import org.springframework.util.ObjectUtils;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zs
 * @since 2023-12-14
 */
public interface TUserMapper extends BaseExtMapper<TUser> {
    /**
     * 验证手机号
     *
     * @param userId
     * @param phone
     * @return
     */
    default TUser checkPhoneNumber(Long userId, String phone) {

        return new LambdaQueryChainWrapper<>(this).ne(!ObjectUtils.isEmpty(userId), TUser::getUserId, userId)
                .eq(TUser::getPhone, phone)
                .last("limit 1")
                .one();
    }
}
