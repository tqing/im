/*
 * Copyright 2013-2022 Xia Jun(3979434@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ***************************************************************************************
 *                                                                                     *
 *                        Website : http://www.farsunset.com                           *
 *                                                                                     *
 ***************************************************************************************
 */
package com.itanqing.qcsdk.acceptor;

import com.itanqing.qcsdk.acceptor.config.WebsocketConfig;
import com.itanqing.qcsdk.coder.json.TextMessageDecoder;
import com.itanqing.qcsdk.coder.json.TextMessageEncoder;
import com.itanqing.qcsdk.coder.protobuf.WebMessageDecoder;
import com.itanqing.qcsdk.coder.protobuf.WebMessageEncoder;
import com.itanqing.qcsdk.constant.WebsocketProtocol;
import com.itanqing.qcsdk.handler.IllegalRequestHandler;
import com.itanqing.qcsdk.handshake.HandshakeHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.concurrent.TimeUnit;

/**
 * websocket协议端口
 * 可针对原生app使用
 */
@ChannelHandler.Sharable
public class WebsocketAcceptor extends NioSocketAcceptor {

	private static final String JSON_BANNER = "\n\n" +
			"* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\n" +
			"*                                                                                   *\n" +
			"*                                                                                   *\n" +
			"*              Websocket Server started on port {} for [JSON] mode.              *\n" +
			"*                                                                                   *\n" +
			"*                                                                                   *\n" +
			"* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\n";

	private static final String PROTOBUF_BANNER = "\n\n" +
			"* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\n" +
			"*                                                                                   *\n" +
			"*                                                                                   *\n" +
			"*             Websocket Server started on port {} for [protobuf] mode.           *\n" +
			"*                                                                                   *\n" +
			"*                                                                                   *\n" +
			"* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\n";

	private final WebsocketConfig config;

	private final HandshakeHandler handshakeHandler;

	private final ChannelHandler illegalRequestHandler = new IllegalRequestHandler();

	public WebsocketAcceptor(WebsocketConfig config){
		super(config);
		this.config = config;

//		System.out.println("配置信息"+JSONUtils.toJSONString(config));
		this.handshakeHandler = new HandshakeHandler(config.getHandshakePredicate());
	}

	/**
	 * bind基于websocket协议的socket端口
	 */
	@Override
	public void bind(){
		logger.info("websocket bing......");

		if (!config.isEnable()){
			return;
		}

		ServerBootstrap bootstrap = createServerBootstrap();
		// 指定childGroup中的eventLoop所绑定的线程所要处理的处理器
		bootstrap.childHandler(new ChannelInitializer<SocketChannel>() {

			@Override
			public void initChannel(SocketChannel ch){
				ch.pipeline().addLast(new HttpServerCodec());//HTTP协议的编解码器
				ch.pipeline().addLast(new ChunkedWriteHandler());  //如果在传输之前还想要对文件进行一定程度的改动则需要使用ChunkedWriteHandler，他可以支持异步写大型数据同时又不会消耗大量的内存  以处理作为ChunkedInput传入的数据，支持异步写大型数据
				ch.pipeline().addLast(new HttpObjectAggregator(4 * 1024));  //正如我们在上面HTTP的请求和响应组成部分图中所看到的，HTTP 的请求和响应可能由许多部分组成，因此需要聚合它们以形成完整的消息，Netty为此提供了HttpObjectAggregator类来简化这一操作，消息分段将被缓冲，直到可以转发一个完整的消息给下一个 ChannelInboundHandler   将最大的消息大小为 4 KB 的 HttpObjectAggregator 添加到 ChannelPipeline
				ch.pipeline().addLast(new WebSocketServerProtocolHandler(config.getPath(),true));
				ch.pipeline().addLast(handshakeHandler);
				if (config.getProtocol() == WebsocketProtocol.JSON){
					ch.pipeline().addLast(new TextMessageDecoder());
					ch.pipeline().addLast(new TextMessageEncoder());
				}else {
					ch.pipeline().addLast(new WebMessageDecoder());
					ch.pipeline().addLast(new WebMessageEncoder());
				}
				ch.pipeline().addLast(new IdleStateHandler(config.getReadIdle().getSeconds(), config.getWriteIdle().getSeconds(), 0, TimeUnit.SECONDS)); //IdleStateHandler类提供心跳检测
				ch.pipeline().addLast(loggingHandler);
				ch.pipeline().addLast(WebsocketAcceptor.this);
				ch.pipeline().addLast(illegalRequestHandler);
			}

		});

		ChannelFuture channelFuture = bootstrap.bind(config.getPort()).syncUninterruptibly();
		channelFuture.channel().newSucceededFuture().addListener(future -> {
			if (config.getProtocol() == WebsocketProtocol.JSON){
				logger.info(JSON_BANNER, config.getPort());
			}
			if (config.getProtocol() == WebsocketProtocol.PROTOBUF){
				logger.info(PROTOBUF_BANNER, config.getPort());
			}
		});
		channelFuture.channel().closeFuture().addListener(future -> this.destroy());
	}

}
