
package com.itanqing.qcim;

import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.xiaohuoren.dubbo.common.utils.BigDecimalUtils;
import com.xiaohuoren.dubbo.mybatis.utils.MyBaseBatisPlusGen;
import org.apache.ibatis.jdbc.ScriptRunner;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author tanqing
 * @title
 * @create 2021-11-03 09:32
 * @since
 */

public class MyBatisPlusGen {

    /**
     * 执行初始化数据库脚本
     */

    public static void before() throws SQLException {
        Connection conn = DATA_SOURCE_CONFIG.build().getConn();
        ScriptRunner scriptRunner = new ScriptRunner(conn);
        scriptRunner.setAutoCommit(true);
        conn.close();
    }


    /**
     * 数据源配置
     */

    private static final DataSourceConfig.Builder DATA_SOURCE_CONFIG = new DataSourceConfig.
            Builder("jdbc:mysql://test.mysql.xiaohuoren.com:3306/easy_admin", "xiaohuoren", "6&nmb8s@RlMs4msO");


    /**
     * 执行 run
     */
    public static void main(String[] args) throws SQLException {
        BigDecimal divide = BigDecimalUtils.divide(new BigDecimal(10), new BigDecimal(3));

        ArrayList<String> tableList = new ArrayList<>();
        tableList.add("t_account");
        tableList.add("t_user");
        tableList.add("t_session");
//        tableList.add("order");
//        tableList.add("order_img");
//        tableList.add("trade_scheme");
//        tableList.add("wallet");
//        tableList.add("trade");
//        tableList.add("strategy_multiple");
//        tableList.add("wallet_record");
//        tableList.add("product");
//        tableList.add("order2");
//        tableList.add("order_item");
//        tableList.add("scheme_info");
//        tableList.add("scheme_plan");
//        tableList.add("scheme_tag");
//        tableList.add("plan");
//        tableList.add("plan_game");
//        tableList.add("plan_game_infer");

//        tableList.add("user_info_Store");
//        tableList.add("user_info_platform");
//        tableList.add("user_info_customer");
//        tableList.add("permission_role_user");
//        tableList.add("shopping_cart");
//        tableList.add("user_info_consumer");
//        tableList.add("user_info_platform");
        MyBaseBatisPlusGen.gen(tableList, "com.itanqing.qcim", "zs", DATA_SOURCE_CONFIG, false);
        System.out.println("生成成功！！！");
    }

}

